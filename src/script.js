let template = Handlebars.compile(`
{{#each this}}
<section class="my-container rounded-3xl flex flex-col md:flex-row border border-grey my-16">
    <article class="relative lg:w-2/5 md:w-1/2 rounded-t-3xl h-56 md:h-auto md:rounded-l-3xl md:rounded-tr-none 
                    bg-center bg-cover" style="background-image: url('/src/images/{{this.image}}')">
        <span class="absolute py-3 px-9 uppercase font-semibold top-8 {{this.badge_style}}">{{this.badge}}</span>
    </article>
    <article class="lg:w-3/5 md:w-1/2 py-5 px-4">
        <h1 class="py-1.5">
            {{this.name}}
        </h1>
        <span class="text-xs md:text-sm before:align-bottom py-1.5 text-light-grey before:pr-1.5
                     before:content-[url('/src/images/clock.svg')]">
            {{this.time}}&nbsp;часа
        </span>
        <ul class="px-4">
        {{#each this.benefits}}
            <li class="pl-4 py-1.5 text-base md:text-lg marker:content-[url('/src/images/marker.svg')]">
                {{this.text}}
                {{#each this.times}}
                    <button role="button" class="rounded-xl px-4 py-1 bg-light-blue leading-4" 
                            onclick="this.classList.toggle('active')">
                    {{this}}
                    </button>
                {{/each}}
            </li>
        {{/each}}
        </ul>
        <div class="flex flex-row items-center">
            <div class="pr-3">
                <p class="text-4xl">{{this.cost}}&nbsp;&#8381;</p>
                {{#if this.last_cost}}
                <span class="text-sm ">{{this.last_cost}}&nbsp;&#8381; на&nbsp;причале</span>
                {{/if}}
            </div>
            <div>
                <a href="#" class="block px-11 py-3.5 rounded-3xl text-lg bg-yellow hover:bg-dark-yellow">
                    Подробнее
                </a>
            </div>
        </div>
    </article>
</section>
{{/each}}
`);
document.getElementById("main").innerHTML = template(cards);