let cards = [
    {
        name: "Обзорная экскурсия по рекам и каналам с остановками Hop on Hop Off 2020",
        time: "2",
        badge: "Новинка",
        badge_style: "bg-sky-blue text-white",
        benefits: [
            {
                text: "Билет на целый день",
                times: []
            },
            {
                text: "Неограниченное число катаний",
                times: []
            },
            {
                text: "6 остановок у главных достопримечательностей",
                times: []
            },
            {
                text: "Ближайший рейс сегодня",
                times: [
                    "9:00", "12:00", "15:00"
                ]
            }
        ],
        cost: 900,
        last_cost: 1200,
        image: "river.png"
    },
    {
        name: "Обзорная экскурсия по рекам и каналам с остановками Hop on Hop Off 2020",
        time: "2",
        badge: "Круглый год",
        badge_style: "bg-yellow text-black",
        benefits: [
            {
                text: "Билет на целый день",
                times: []
            },
            {
                text: "Неограниченное число катаний",
                times: []
            },
            {
                text: "6 остановок у главных достопримечательностей",
                times: []
            },
            {
                text: "Ближайший рейс сегодня",
                times: [
                    "9:00", "12:00", "15:00"
                ]
            }
        ],
        cost: 900,
        last_cost: null,
        image: "spas_na_krovi.png"
    },
    {
        name: "Обзорная экскурсия по рекам и каналам с остановками Hop on Hop Off 2020",
        time: "2",
        badge: "",
        badge_style: "",
        benefits: [
            {
                text: "Билет на целый день",
                times: []
            },
            {
                text: "Неограниченное число катаний",
                times: []
            },
            {
                text: "6 остановок у главных достопримечательностей",
                times: []
            },
            {
                text: "Ближайший рейс сегодня",
                times: [
                    "9:00", "12:00", "15:00"
                ]
            }
        ],
        cost: 900,
        last_cost: 1200,
        image: "piano.png"
    },
]