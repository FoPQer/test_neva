/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./src/*.js", "index.html"],
    theme: {
        fontFamily: {
            openSans: ['Open Sans', 'sans-serif']
        },
        colors: {
            'semi-black': '#343434',
            'grey': '#8e8e8e',
            'light-grey': '#9e9e9e',
            'blue': '#6BA6FF',
            'light-blue': '#C5DCFF',
            'sky-blue': '#099CE8',
            'yellow': '#FFD83C',
            'dark-yellow': '#facc15',
            'white': '#ffffff'
        }
    },
    extend: {
        theme: {
            colors: {}
        }
    },
    plugins: [],
}
